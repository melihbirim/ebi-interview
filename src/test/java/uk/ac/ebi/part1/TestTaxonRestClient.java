package uk.ac.ebi.part1;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.net.httpserver.HttpServer;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import uk.ac.ebi.part1.models.Taxon;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXB;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;

/**
 * Created by melih on 1/29/2015.
 */
public class TestTaxonRestClient {

    Logger logger = Logger.getLogger(this.getClass().getName());

    static URI uri = null;
    static HttpServer httpServer = null;

    @Test
    public void testURIAndServer() {
        logger.info("[testURIAndServer] started");
        Assert.assertNotNull(uri);
        Assert.assertNotNull(httpServer);
    }

    @Test
    public void testTaxonJSONRequest() {
        logger.info("[testTaxonJSONRequest] started");
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(uri);

        WebResource path = service.path("json/168015");

        Taxon response = path.accept(MediaType.APPLICATION_JSON).get(Taxon.class);

        Assert.assertEquals(response.getName(), "Aotinae gen. sp.");

        logger.debug("response = " + response);

        String responseStr = path.accept(MediaType.APPLICATION_JSON).get(String.class);

        logger.debug("response = " + responseStr);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Taxon unmarshal = objectMapper.readValue(responseStr,Taxon.class);

            Assert.assertEquals(unmarshal, response);

        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }


    }

    @Test
    public void testTaxonXMLRequest() {
        logger.info("[testTaxonJSONRequest] started");
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(uri);

        WebResource path = service.path("xml/168015");

        Taxon response = path.accept(MediaType.APPLICATION_XML).get(Taxon.class);

        Assert.assertEquals(response.getName(), "Aotinae gen. sp.");

        String responseStr = path.accept(MediaType.APPLICATION_XML).get(String.class);

        logger.debug("response = " + responseStr);

        Taxon unmarshal = JAXB.unmarshal(new StringReader(responseStr), Taxon.class);

        Assert.assertEquals(unmarshal, response);
    }

    @Before
    public void before() {
        try {
            System.out.println("[before] Starting Taxon's Embedded Jersey HTTPServer...\n");
            if (httpServer == null) {
                httpServer = createHttpServer();
                httpServer.start();
                uri = getURI();
                Assert.assertNotNull(uri);
                logger.debug("[before] Started Embedded Jersey HTTPServer Successfully !!!");
            } else {
                logger.debug("[before] HTTP Server already started, skip it [OK]");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    /**
     * Creates and embedded jetty server
     * @return
     * @throws IOException
     */
    private static HttpServer createHttpServer() throws IOException {
        ResourceConfig resourceConfig = new PackagesResourceConfig("uk.ac.ebi.part1.rest");
        return HttpServerFactory.create(getURI(), resourceConfig);
    }

    private static URI getURI() {
        return UriBuilder.fromUri("http://" + getHostName() + "/").port(8090).build();
    }

    private static String getHostName() {
        String hostName = "localhost";
        /*try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }*/
        return hostName;
    }
}
