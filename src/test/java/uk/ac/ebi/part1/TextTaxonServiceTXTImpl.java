package uk.ac.ebi.part1;

import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import uk.ac.ebi.part1.dao.TaxonService;
import uk.ac.ebi.part1.dao.TaxonServiceTXTImpl;
import uk.ac.ebi.part1.models.Taxon;

import java.util.concurrent.TimeUnit;

//@PerfTest(invocations = 1000000)
public class TextTaxonServiceTXTImpl {

    @Rule
    public ContiPerfRule i = new ContiPerfRule();

    Logger logger = Logger.getLogger(this.getClass().getName());
    long startTime = 0;

    @Before
    public void before() {
        startTime = System.nanoTime();
    }

    @After
    public void after() {
        long estimatedTime = System.nanoTime() - startTime;
        logger.debug("Estimated [TIME" + estimatedTime + " ns] [" + TimeUnit.NANOSECONDS.toMillis(estimatedTime) + " ms]");
    }

    @Test
    public void testReadFile() {
        TaxonService instance = TaxonServiceTXTImpl.getInstance();
        Assert.assertNotNull(instance);
        Taxon taxon = instance.getById("905053");
        Assert.assertNotNull(taxon);
        Assert.assertNotSame(taxon.getName(), "Japanese monkeys");

    }
}
