package uk.ac.ebi.part3;

import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;
import uk.ac.ebi.part1.models.Taxon;

public class Part3SolutionTest extends TestCase {

    @Test
    public void testGetTaxonArray() throws Exception {
        BaseTaxonReader abs = new ArrayBasedTaxonReader();
        Taxon taxon = abs.getById("905053");
        Assert.assertNotNull(taxon);
        //Assert.assertEquals("Tarsius sangirensis", taxon.getName());
    }


    @Test
    public void testGetTaxonHash() throws Exception {
        BaseTaxonReader abs = new HashBasedTaxonReader();
        Taxon taxon = abs.getById("905053");
        Assert.assertNotNull(taxon);
        Assert.assertEquals("Tarsius sangirensis", taxon.getName());
    }


}