tax_id	scientific_name
57118	unclassified Primates
36513	Japanese monkeys
38020	marmosets
70699	unidentified monkey
976298	Primates sp. BOLD:AAA0001
976299	Primates sp. BOLD:AAC9009
976300	Primates sp. BOLD:AAE4744
976301	Primates sp. BOLD:AAJ7304
976302	Primates sp. BOLD:AAO0053
1068292	Primates sp. BOLD:AAC7735
1068293	Primates sp. BOLD:AAD0215
1068294	Primates sp. BOLD:AAF0367
9447	Lemur catta
69491	Lemur sp.
9455	Varecia variegata
87289	Varecia variegata variegata
554167	Varecia rubra
13514	Eulemur coronatus
13515	Eulemur fulvus
27680	Eulemur fulvus mayottensis
40322	Eulemur fulvus fulvus
47178	Eulemur fulvus collaris
122224	Eulemur fulvus albocollaris
30602	Eulemur macaco
30603	Eulemur macaco macaco
34828	Eulemur mongoz
34829	Eulemur rubriventer
87288	Eulemur flavifrons
122225	Eulemur sanfordi
639525	Eulemur cinereiceps
859983	Eulemur rufus
859984	Eulemur rufifrons
1215604	Eulemur albifrons
1417238	Eulemur hybrids
1417231	Eulemur albifrons x rufifrons
1417236	Eulemur albifrons x cinereiceps
1417237	Eulemur cinereiceps x rufifrons
13557	Hapalemur griseus
122219	Hapalemur griseus griseus
122220	Hapalemur griseus alaotrensis
122221	Hapalemur griseus occidentalis
185456	Hapalemur griseus meridionalis
185596	Hapalemur griseus JF2002
122222	Hapalemur aureus
867377	Hapalemur occidentalis
1328070	Prolemur simus
1513475	Pachylemur jullyi
30601	Propithecus tattersalli
34825	Propithecus verreauxi
122229	Propithecus verreauxi verreauxi
39582	Propithecus sp.
83281	Propithecus diadema
171945	Propithecus diadema diadema
171947	Propithecus diadema marshi
379532	Propithecus coquereli
475618	Propithecus deckenii
475619	Propithecus deckenii coronatus
481705	Propithecus deckenii deckenii
543559	Propithecus edwardsi
543560	Propithecus candidus
989338	Propithecus perrieri
34827	Indri indri
122246	Avahi laniger
132108	Avahi occidentalis
402239	Avahi unicolor
402244	Avahi cleesei
1313323	Avahi peyrierasi
523821	Archaeolemur majori
523822	Archaeolemur edwardsi
523825	Hadropithecus stenognathus
9453	Lepilemur mustelinus
78583	Lepilemur dorsalis
78584	Lepilemur septentrionalis
78866	Lepilemur ruficaudatus
100475	Lepilemur leucopus
122230	Lepilemur edwardsi
185457	Lepilemur microdon
185458	Lepilemur sp. RANO236
185590	Lepilemur sp. RANO234
185591	Lepilemur sp. RANO235
236287	Lepilemur mitsinjoensis
236288	Lepilemur seali
342398	Lepilemur sahamalazensis
342399	Lepilemur aeeclis
342401	Lepilemur ankaranensis
378517	Lepilemur mittermeieri
400023	Lepilemur sp. hubbardi
458171	Lepilemur manasamody
458173	Lepilemur otto
486960	Lepilemur jamesi
756882	Lepilemur hubbardorum
886964	Lepilemur petteri
886965	Lepilemur randrianasoloi
126594	Megaladapis sp.
185953	Megaladapis edwardsi
322020	Megaladapis sp. UA5482
322021	Megaladapis sp. UA4823
322022	Megaladapis sp. UA4821
322023	Megaladapis sp. UA4822
9460	Cheirogaleus medius
47177	Cheirogaleus major
291259	Cheirogaleus crossleyi
752538	Cheirogaleus sibreei
867375	Cheirogaleus minusculus
30608	Microcebus murinus
122231	Microcebus ravelobensis
122232	Microcebus rufus
143283	Microcebus myoxinus
143351	Microcebus tavaratra
143352	Microcebus berthae
143353	Microcebus sambiranensis
143354	Microcebus griseorufus
236272	Microcebus simmonsi
236274	Microcebus mittermeieri
236275	Microcebus jollyae
270538	Microcebus sp. EELHDZ M98
270540	Microcebus sp. EELHDZ TAD23
270674	Microcebus sp. EELHDZ VEV33
270675	Microcebus sp. EELHDZ VEV35
343908	Microcebus lehilahytsara
388736	Microcebus sp. ANT5.1
412750	Microcebus bongolavensis
412751	Microcebus danfossi
412752	Microcebus lokobensis
412758	Microcebus sp. Ambongabe
412759	Microcebus sp. Bora
412760	Microcebus sp. Mahajamba-Est
412761	Microcebus sp. Maroakata
415757	Microcebus mamiratra
548481	Microcebus macarthurii
548482	Microcebus sp. Anjiahely
764313	Microcebus sp. DWW-2010a
864580	Microcebus arnholdi
864581	Microcebus margotmarshae
1131379	Microcebus gerpi
1310123	Microcebus tanosi
1310124	Microcebus marohita
1310125	Microcebus sp. d'Ambre
47180	Mirza coquereli
339999	Mirza zaza
415977	Mirza sp. 001y03
122248	Allocebus trichotis
261734	Phaner furcifer
568393	Phaner pallescens
1313324	Phaner electromontis
1313325	Phaner parienti
322025	Palaeopropithecus sp. UA4466
322026	Palaeopropithecus sp. UA6184
322027	Palaeopropithecus sp. KPK-2005
1513477	Palaeopropithecus ingens
1597978	Palaeopropithecus maximus
9468	Loris tardigradus
261740	Loris tardigradus nordicus
300163	Loris lydekkerianus
300166	Loris lydekkerianus malabaricus
9470	Nycticebus coucang
310933	Nycticebus coucang coucang
101277	Nycticebus intermedius
101278	Nycticebus pygmaeus
108082	Nycticebus sp.
261741	Nycticebus bengalensis
310932	Nycticebus menagensis
310934	Nycticebus javanicus
9472	Perodicticus potto
9473	Perodicticus potto edwarsi
261737	Perodicticus potto ibeanus
261739	Arctocebus calabarensis
300161	Arctocebus aureus
31869	Daubentonia madagascariensis
9464	Galago sp.
9465	Galago senegalensis
30609	Galago moholi
34830	Galago alleni
111173	Galago gallarum
135486	Galago matschiei
261731	Galago gabonensis
261732	Galago granti
867376	Galago thomasi
9463	Otolemur crassicaudatus
30611	Otolemur garnettii
477174	Otolemur garnettii garnettii
337212	Otolemur monteiri
337213	Otolemur monteiri monteiri
337214	Otolemur monteiri argentatus
89672	Galagoides demidoff
111174	Galagoides zanzibaricus
337210	Galagoides orinus
1487601	Galagoides cocos
261736	Euoticus elegantulus
314293	Simiiformes
9482	Callithrix argentata
867362	Callithrix argentata argentata
9483	Callithrix jacchus
9485	Callithrix sp.
9493	Callithrix pygmaea
48842	Callithrix emiliae
52231	Callithrix geoffroyi
52232	Callithrix humeralifera
57375	Callithrix aurita
57377	Callithrix mauesi
57378	Callithrix penicillata
198701	Callithrix cf. emiliae
198702	Callithrix saterei
217956	Callithrix sp. DB-2003
666519	Callithrix humilis
867363	Callithrix kuhlii
1090896	Callithrix melanura
1127763	Callithrix rondoni
1346251	Callithrix flaviceps
1476500	Callithrix jacchus x penicillata
9487	Saguinus fuscicollis
48018	Saguinus fuscicollis fuscicollis
343424	Saguinus fuscicollis weddelli
881946	Saguinus fuscicollis leucogenys
881947	Saguinus fuscicollis illigeri
881949	Saguinus fuscicollis nigrifrons
881950	Saguinus fuscicollis lagonotus
9488	Saguinus mystax
873069	Saguinus mystax mystax
9489	Saguinus nigricollis
873070	Saguinus nigricollis nigricollis
9490	Saguinus oedipus
9491	Saguinus imperator
881951	Saguinus imperator subgrisescens
30586	Saguinus midas
78354	Saguinus midas midas
37588	Saguinus bicolor
37589	Saguinus bicolor bicolor
43778	Saguinus geoffroyi
78454	Saguinus labiatus
873066	Saguinus labiatus labiatus
100754	Saguinus sp.
122388	Saguinus tripartitus
260575	Saguinus sp. T1030
290597	Saguinus leucopus
356665	Saguinus niger
356666	Saguinus martinsi
873067	Saguinus melanoleucus
873068	Saguinus melanoleucus melanoleucus
873073	Saguinus graellsi
1079039	Saguinus inustus
9495	Callimico goeldii
1090914	Callimico sp. MAS-2011
30588	Leontopithecus rosalia
57374	Leontopithecus chrysomelas
58710	Leontopithecus chrysopygus
9514	Cebus albifrons
9516	Cebus capucinus
9517	Cebus sp.
37294	Cebus kaapori
37295	Cebus olivaceus
1090893	Cebus olivaceus nigrivittatus
1090916	Cebus sp. MAS-2011
9515	Sapajus apella
174598	Sapajus apella apella
1547595	Sapajus apella macrocephalus
174599	Sapajus xanthosternos
649471	Sapajus cay
867366	Sapajus nigritus
867367	Sapajus nigritus robustus
1112861	Sapajus flavius
1126382	Sapajus libidinosus
168015	Aotinae gen. sp.
9521	Saimiri sciureus
190117	Saimiri sciureus sciureus
198114	Saimiri sciureus collinsi
495298	Saimiri sciureus albigena
495299	Saimiri sciureus cassiquiarensis
495300	Saimiri sciureus macrodon
27679	Saimiri boliviensis
39432	Saimiri boliviensis boliviensis
495297	Saimiri boliviensis peruviensis
66265	Saimiri ustus
70928	Saimiri oerstedii
867380	Saimiri oerstedii oerstedii
942008	Saimiri oerstedii citrinellus
1090919	Saimiri sp. MAS-2011
1561161	Saimiri vanzolinii
1561164	Saimiri sp.
9505	Aotus trivirgatus
30591	Aotus azarai
120088	Aotus azarai azarai
280755	Aotus azarai boliviensis
867331	Aotus azarai infulatus
37293	Aotus nancymaae
43147	Aotus lemurinus
57175	Aotus nigriceps
57176	Aotus vociferans
222417	Aotus sp. NIM-2003
231953	Aotus sp.
261316	Aotus sp. PDE-2004
292213	Aotus griseimembra
361674	Aotus brumbacki
413234	Aotus sp. Aot1
940829	Aotus sp. SHM-2010
1002694	Aotus sp. AVB-2011
1090913	Aotus sp. MAS-2011
1230482	Aotus sp. LC-2012
1263727	Aotus zonalis
9525	Chiropotes satanas
198627	Chiropotes albinasus
202458	Chiropotes satanas x albinasus
280160	Chiropotes utahickae
280163	Chiropotes israelita
626367	Chiropotes albinasus x satanas
658221	Chiropotes chiropotes
1560631	Chiropotes chiropotes sagulata
1090917	Chiropotes sp. MAS-2011
30596	Cacajao calvus
85699	Cacajao calvus calvus
658398	Cacajao calvus novaesi
658399	Cacajao calvus ucayalli
658400	Cacajao calvus rubicundus
70825	Cacajao melanocephalus
70927	Cacajao rubicundus
535896	Cacajao ayresi
535897	Cacajao hosomi
30598	Pithecia irrorata
43777	Pithecia pithecia
285955	Pithecia pithecia pithecia
595220	Pithecia monachus
9523	Callicebus moloch
30592	Callicebus torquatus
70814	Callicebus personatus
78274	Callicebus personatus nigrifrons
78275	Callicebus personatus personatus
78255	Callicebus hoffmannsi
116962	Callicebus sp.
202457	Callicebus cupreus
210166	Callicebus lugens
221704	Callicebus brunneus
230833	Callicebus donacophilus
285954	Callicebus donacophilus donacophilus
867332	Callicebus caligatus
867333	Callicebus coimbrai
867334	Callicebus nigrifrons
1090915	Callicebus sp. MAS-2011
1560619	Callicebus purinus
9502	Alouatta caraya
9503	Alouatta seniculus
182254	Alouatta seniculus seniculus
198115	Alouatta seniculus macconnelli
30589	Alouatta palliata
182248	Alouatta palliata mexicana
182249	Alouatta palliata coibensis
182250	Alouatta palliata palliata
182251	Alouatta palliata trabeata
182252	Alouatta palliata aequatorialis
30590	Alouatta belzebul
182257	Alouatta belzebul belzebul
121123	Alouatta sara
163110	Alouatta stramineus
182253	Alouatta pigra
182256	Alouatta guariba
183328	Alouatta guariba clamitans
9507	Ateles belzebuth
118643	Ateles belzebuth chamek
118644	Ateles belzebuth marginatus
9508	Ateles fusciceps
183327	Ateles fusciceps robustus
1497546	Ateles fusciceps rufiventris
9509	Ateles geoffroyi
118647	Ateles geoffroyi panamensis
118648	Ateles geoffroyi vellerosus
118649	Ateles geoffroyi yucatanensis
795835	Ateles geoffroyi frontatus
1214775	Ateles geoffroyi ornatus
1497547	Ateles geoffroyi azuerensis
9510	Ateles paniscus
9511	Ateles sp.
36234	Ateles paniscus x Ateles fusciceps
119628	Ateles sp. haplotype Abh9
119629	Ateles sp. haplotype Abh10
119630	Ateles sp. haplotype Abh11
119631	Ateles sp. haplotype Agsubspp. 16
119632	Ateles sp. haplotype Afr19
119633	Ateles sp. haplotype Afr20
119634	Ateles sp. haplotype Afr21
119638	Ateles sp. haplotype Abh19
119639	Ateles sp. haplotype Agp13
119640	Ateles sp. haplotype Afr27
269772	Ateles sp. KO-2004
278713	Ateles sp. JPV-2004
129801	Ateles hybridus
1230481	Ateles hybridus hybridus
1529884	Ateles marginatus
9519	Lagothrix lagotricha
638937	Lagothrix sp. CCS-2009a
767355	Lagothrix lugens
767356	Lagothrix poeppigii
767357	Lagothrix cana
30594	Brachyteles arachnoides
342807	Brachyteles hypoxanthus
314294	Cercopithecoidea
9530	Cercocebus torquatus
81944	Cercocebus torquatus torquatus
9531	Cercocebus atys
75570	Cercocebus atys lunulatus
1592215	Cercocebus atys atys
9532	Cercocebus galeritus
75569	Cercocebus chrysogaster
255237	Cercocebus agilis
9535	Cercopithecus cephus
167131	Cercopithecus cephus ngottoensis
192859	Cercopithecus cephus cephodes
192860	Cercopithecus cephus cephus
9536	Cercopithecus hamlyni
867373	Cercopithecus hamlyni hamlyni
36223	Cercopithecus ascanius
192855	Cercopithecus ascanius ascanius
192856	Cercopithecus ascanius katangae
192857	Cercopithecus ascanius schmidti
192858	Cercopithecus ascanius whitesidei
36224	Cercopithecus diana
36225	Cercopithecus mitis
1137473	Cercopithecus mitis boutourlinii
1137480	Cercopithecus mitis heymansi
1137482	Cercopithecus mitis mitis
1137490	Cercopithecus mitis opisthostictus
1137491	Cercopithecus mitis stuhlmanni
36226	Cercopithecus mona
100489	Cercopithecus mona mona
192861	Cercopithecus mona campbelli
36227	Cercopithecus neglectus
36228	Cercopithecus nictitans
1137494	Cercopithecus nictitans martini
1137495	Cercopithecus nictitans nictitans
100224	Cercopithecus lhoesti
100487	Cercopithecus petaurista
192862	Cercopithecus petaurista petaurista
192893	Cercopithecus petaurista buettikoferi
102108	Cercopithecus pogonias
192863	Cercopithecus pogonias grayi
1137506	Cercopithecus pogonias nigripes
1137507	Cercopithecus pogonias pogonias
1137509	Cercopithecus pogonias schwarzianus
147649	Cercopithecus preussi
1137512	Cercopithecus preussi insularis
1137513	Cercopithecus preussi preussi
147650	Cercopithecus solatus
161496	Cercopithecus erythrogaster
304409	Cercopithecus erythrogaster erythrogaster
1137433	Cercopithecus erythrogaster pococki
167130	Cercopithecus erythrotis
1137434	Cercopithecus erythrotis erythrotis
1137435	Cercopithecus erythrotis camerunensis
263448	Cercopithecus wolfi
867374	Cercopithecus wolfi wolfi
1137505	Cercopithecus wolfi elegans
1137508	Cercopithecus wolfi pyrogaster
303588	Cercopithecus campbelli
304410	Cercopithecus campbelli lowei
867370	Cercopithecus albogularis
867372	Cercopithecus albogularis kolbi
1137444	Cercopithecus albogularis albotorquatus
1137476	Cercopithecus albogularis erythrarchus
1137483	Cercopithecus albogularis francescae
1137484	Cercopithecus albogularis labiatus
1137488	Cercopithecus albogularis moloneyi
1137489	Cercopithecus albogularis monoides
1137048	Cercopithecus dryas
1137049	Cercopithecus roloway
1137474	Cercopithecus doggetti
1137481	Cercopithecus kandti
1137511	Cercopithecus denti
1144881	Cercopithecus sp. WMS-2012
1191211	Cercopithecus lomamiensis
9538	Erythrocebus patas
9540	Macaca arctoides
9541	Macaca fascicularis
90386	Macaca fascicularis philippinensis
1215360	Macaca fascicularis fascicularis
9542	Macaca fuscata
9543	Macaca fuscata fuscata
179089	Macaca fuscata yakui
9544	Macaca mulatta
1449913	Macaca mulatta lasiotus
9545	Macaca nemestrina
90387	Macaca nemestrina leonina
90388	Macaca nemestrina nemestrina
244255	Macaca nemestrina siberu
9546	Macaca sylvanus
9548	Macaca radiata
439030	Macaca radiata radiata
9549	Macaca sp.
9551	Macaca assamensis
1411634	Macaca assamensis assamensis
9552	Macaca sinica
9553	Macaca speciosa
40843	Macaca tonkeana
54600	Macaca nigra
54601	Macaca silenus
54602	Macaca thibetana
257877	Macaca thibetana thibetana
78449	Macaca cyclopis
90381	Macaca brunnescens
90382	Macaca hecki
90383	Macaca maura
90384	Macaca nigrescens
90385	Macaca ochreata
168259	Macaca maura x tonkeana
171293	Macaca hecki x tonkeana
230653	Macaca pagensis
281404	Macaca balantak
281405	Macaca balantak x tonkeana
285350	Macaca leonina
402888	Macaca munzala
867378	Macaca siberu
9555	Papio anubis
211508	Papio anubis anubis
9556	Papio cynocephalus
106398	Papio cynocephalus cynocephalus
501791	Papio cynocephalus ibeanus
9557	Papio hamadryas
9562	Papio hamadryas hamadryas
36229	Papio ursinus
208089	Papio ursinus ursinus
61183	Papio sp.
100937	Papio papio
208091	Papio kindae
208510	Papio cynocephalus x Papio anubis
285280	Papio sp. Africa-2004
425246	Papio sp. PS-2007
1560552	Papio anubis x hamadryas
9565	Theropithecus gelada
9561	Mandrillus sphinx
9568	Mandrillus leucophaeus
36231	Miopithecus talapoin
560240	Miopithecus talapoin talapoin
100488	Miopithecus ogouensis
54135	Allenopithecus nigroviridis
75566	Lophocebus aterrimus
75567	Lophocebus albigena
75568	Lophocebus albigena albigena
371039	Rungwecebus kipunji
9534	Chlorocebus aethiops
100936	Chlorocebus aethiops vervet
101841	Chlorocebus aethiops aethiops
60710	Chlorocebus pygerythrus
460674	Chlorocebus pygerythrus pygerythrus
508712	Chlorocebus pygerythrus hilgerti
60711	Chlorocebus sabaeus
60712	Chlorocebus tantalus
508710	Chlorocebus tantalus budgetti
460675	Chlorocebus cynosuros
1090918	Chlorocebus sp. MAS-2011
1284215	Chlorocebus djamdjamensis
1284216	Chlorocebus sp. TH-2013
9572	Colobus polykomos
33548	Colobus guereza
132548	Colobus guereza kikuyuensis
373030	Colobus guereza matschiei
373031	Colobus guereza caudatus
373032	Colobus guereza occidentalis
34824	Colobus sp.
54131	Colobus angolensis
336983	Colobus angolensis palliatus
288544	Colobus sp. DTG-2004
325172	Colobus sp. MB-2005
378195	Colobus vellerosus
378196	Colobus vellerosus x angolensis
517012	Colobus satanas
517013	Colobus satanas satanas
66055	Presbytis senex
78451	Presbytis melalophos
272115	Presbytis melalophos mitrata
1046095	Presbytis melalophos bicolor
1046096	Presbytis melalophos melalophos
1046097	Presbytis melalophos sumatranus
78452	Presbytis comata
272114	Presbytis comata comata
1046091	Presbytis comata fredericae
78453	Presbytis femoralis
1046090	Presbytis chrysomelas
1046092	Presbytis frontata
1046093	Presbytis hosei
1046094	Presbytis hosei hosei
1046098	Presbytis potenziani
1046099	Presbytis potenziani potenziani
1046100	Presbytis potenziani siberu
1046101	Presbytis rubicunda
1046102	Presbytis rubicunda rubicunda
1046103	Presbytis thomasi
43780	Nasalis larvatus
54133	Pygathrix nemaeus
303485	Pygathrix nemaeus nemaeus
310352	Pygathrix nigripes
693712	Pygathrix cinerea
1194332	Pygathrix cinerea 1 RL-2012
1194333	Pygathrix cinerea 2 RL-2012
54137	Trachypithecus vetulus
614071	Trachypithecus vetulus vetulus
54180	Trachypithecus francoisi
272119	Trachypithecus francoisi francoisi
54181	Trachypithecus obscurus
61618	Trachypithecus phayrei
272120	Trachypithecus phayrei phayrei
272121	Trachypithecus phayrei crepuscula
1118394	Trachypithecus phayrei holotephreus
1268362	Trachypithecus phayrei shanicus
66063	Trachypithecus johnii
122765	Trachypithecus cristatus
164650	Trachypithecus geei
164651	Trachypithecus pileatus
222416	Trachypithecus auratus
36233	Trachypithecus auratus pyrrhus
272118	Trachypithecus auratus auratus
449596	Trachypithecus auratus mauritius
271260	Trachypithecus germaini
271261	Trachypithecus barbei
465717	Trachypithecus delacouri
465718	Trachypithecus laotum
465719	Trachypithecus poliocephalus
662738	Trachypithecus poliocephalus leucocephalus
1118393	Trachypithecus poliocephalus poliocephalus
580599	Trachypithecus sp.
744437	Trachypithecus francoisi x poliocephalus
867383	Trachypithecus hatinhensis
1042121	Trachypithecus shortridgei
88029	Semnopithecus entellus
272117	Semnopithecus entellus priam
867381	Semnopithecus entellus entellus
867382	Semnopithecus hector
1208733	Semnopithecus priam
1208734	Semnopithecus hypoleucos
373033	Procolobus verus
61621	Rhinopithecus bieti
61622	Rhinopithecus roxellana
66062	Rhinopithecus avunculus
224329	Rhinopithecus brelichi
1194334	Rhinopithecus bieti 1 RL-2012
1194335	Rhinopithecus bieti 2 RL-2012
1194336	Rhinopithecus strykeri
164648	Piliocolobus badius
491852	Piliocolobus badius badius
491853	Piliocolobus badius temminckii
591933	Piliocolobus gordonorum
591934	Piliocolobus rufomitratus
591935	Piliocolobus tholloni
591936	Piliocolobus tephrosceles
591937	Piliocolobus kirkii
591938	Piliocolobus foai
591944	Piliocolobus pennantii
591945	Piliocolobus preussi
170207	Simias concolor
9576	Cercopithecidae gen. sp.
314295	Hominoidea
9579	Hylobates agilis
9583	Hylobates agilis unko
716690	Hylobates agilis agilis
9580	Hylobates lar
716695	Hylobates lar carpenteri
716696	Hylobates lar entelloides
716697	Hylobates lar lar
716698	Hylobates lar vestitus
9581	Hylobates sp.
9587	Hylobates klossii
9588	Hylobates muelleri
716692	Hylobates muelleri muelleri
716693	Hylobates muelleri funereus
716694	Hylobates muelleri abbotti
9589	Hylobates pileatus
81572	Hylobates moloch
150249	Hylobates sp. TIB-201
280856	Hylobates sp. IGL-2004
288892	Hylobates sp. AMA-2004
419588	Hylobates sp. ECACC MLA144
425245	Hylobates sp. PS-2007
716691	Hylobates alibarbis
9586	Nomascus siki
29089	Nomascus concolor
423449	Nomascus concolor jingdongensis
423450	Nomascus concolor furvogaster
716684	Nomascus concolor concolor
716685	Nomascus concolor lu
61852	Nomascus gabriellae
61853	Nomascus leucogenys
327374	Nomascus nasutus
693984	Nomascus hainanus
9590	Symphalangus syndactylus
405039	Symphalangus syndactylus syndactylus
61851	Hoolock hoolock
593543	Hoolock leuconedys
207598	Homininae
9593	Gorilla gorilla
9595	Gorilla gorilla gorilla
183511	Gorilla gorilla uellensis
406788	Gorilla gorilla diehli
499232	Gorilla beringei
46359	Gorilla beringei graueri
1159185	Gorilla beringei beringei
9597	Pan paniscus
9598	Pan troglodytes
37010	Pan troglodytes schweinfurthii
37011	Pan troglodytes troglodytes
37012	Pan troglodytes verus
91950	Pan troglodytes vellerosus
756884	Pan troglodytes ellioti
1294088	Pan troglodytes verus x troglodytes
9606	Homo sapiens
63221	Homo sapiens neanderthalensis
741158	Homo sapiens ssp. Denisova
1425170	Homo heidelbergensis
607660	Ponginae
9600	Pongo pygmaeus
9602	Pongo pygmaeus pygmaeus
9601	Pongo abelii
9603	Pongo sp.
502961	Pongo abelii x pygmaeus
9477	Tarsius bancanus
9478	Tarsius syrichta
30612	Tarsius sp.
449501	Tarsius dentatus
630277	Tarsius lariang
642591	Tarsius dentatus x lariang
662464	Tarsius tarsier
662655	Tarsius sp. FFA-2009a
905053	Tarsius sangirensis
981131	Tarsius wallacei