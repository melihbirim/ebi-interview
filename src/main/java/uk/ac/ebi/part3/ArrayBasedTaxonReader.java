package uk.ac.ebi.part3;

import uk.ac.ebi.part1.models.Taxon;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by melih on 2/2/2015.
 */
public class ArrayBasedTaxonReader extends BaseTaxonReader {

    public static final int SIZE = 1000000;
    private Taxon[] taxons;
    private int index;
    //private ArrayList<String> taxons;

    public ArrayBasedTaxonReader() {
        try {
            //taxons = new ArrayList<>();
            taxons = new Taxon[SIZE];
            init();

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws IOException, URISyntaxException {
        for (int i = 1; i < SIZE; i++) {
            String cc = i+"\t"+i;
            parse(cc.toCharArray());
        }
    }

    @Override
    public void parse(char[] arr) {
        String tmp = new String(arr);
        String[] token = tmp.split("\t");
        if (token.length == 2) {
            Taxon t = new Taxon(token[0], token[1]);
            taxons[Integer.valueOf(token[0])]= t;
        }
    }

    @Override
    public Taxon getById(String id) {
        Taxon taxon = null;
        return taxons[Integer.valueOf(id)];
        /*for (int i = taxons.size() - 1; i >= 0; i--) {
            taxon = taxons.get(i);
            if (taxon.getId().equals(id)) return taxon;
        }
        return null;*/
    }

}
