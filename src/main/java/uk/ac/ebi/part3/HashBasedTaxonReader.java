package uk.ac.ebi.part3;

import uk.ac.ebi.part1.models.Taxon;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Hashtable;

/**
 * Created by melih on 2/2/2015.
 */
public class HashBasedTaxonReader extends BaseTaxonReader {

    private Hashtable<String, Taxon> taxonMap;

    public HashBasedTaxonReader() {
        taxonMap = new Hashtable<>();
        try {
            init();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parse(char[] arr) {
        String tmp = new String(arr);
        String[] token = tmp.split("\t");
        if (token.length == 2)
            taxonMap.put(token[0], new Taxon(token[0], token[1]));
    }

    @Override
    public Taxon getById(String id) {
        return taxonMap.get(id);
    }

}
