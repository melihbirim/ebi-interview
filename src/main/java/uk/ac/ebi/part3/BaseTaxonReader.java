package uk.ac.ebi.part3;

import uk.ac.ebi.part1.models.Taxon;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by melih on 2/2/2015.
 */
public abstract class BaseTaxonReader {
    public static final String TAXA_TXT = "/taxa.txt";

    public void init() throws IOException, URISyntaxException {
        RandomAccessFile aFile = new RandomAccessFile(this.getClass().getResource(TAXA_TXT).toURI().getPath(), "r");
        FileChannel inChannel = aFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // parse each line with bytes with nio
        CharArrayWriter writer = new CharArrayWriter();

        while (inChannel.read(buffer) > 0) {
            buffer.flip();
            for (int i = 0; i < buffer.limit(); i++) {
                byte b = buffer.get();
                if (b == '\n' || b == '\r') {
                    parse(writer.toCharArray());
                    writer.reset();
                } else {
                    writer.append((char) b);
                }
            }
            buffer.clear(); // do something with the data and clear/compact it.
        }
        inChannel.close();
        aFile.close();
    }
    public abstract void parse(char[] chars);

    public abstract Taxon getById(String id);
}
