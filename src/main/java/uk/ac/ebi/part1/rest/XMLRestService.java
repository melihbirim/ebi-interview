package uk.ac.ebi.part1.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import uk.ac.ebi.part1.models.Taxon;

@Path("/xml")
public class XMLRestService extends JSONRestService {

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Taxon getTaxon(@PathParam("id") String id) {
        return super.getTaxon(id);
    }

}