package uk.ac.ebi.part1.rest;

import uk.ac.ebi.part1.dao.TaxonService;
import uk.ac.ebi.part1.dao.TaxonServiceTXTImpl;
import uk.ac.ebi.part1.models.Taxon;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by melih on 2/2/2015.
 */
@Path("/json")
public class JSONRestService {

    TaxonService instance = TaxonServiceTXTImpl.getInstance();

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Taxon getTaxon(@PathParam("id") String id) {
        return instance.getById(id);
    }

}
