package uk.ac.ebi.part1.models;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {
        "id",
        "name"
})
public class Taxon implements Comparable{

    String id;
    String name;

    public Taxon(){}

    public Taxon(char[] id, char[] name){
        this(new String(id), new String(name));
    }

    public Taxon(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", name:'" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Taxon taxon = (Taxon) o;

        if (id != null ? !id.equals(taxon.id) : taxon.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }


    @Override
    public int compareTo(Object o) {
        return id.compareTo(((Taxon)o).getId());
    }
}
