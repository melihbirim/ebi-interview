package uk.ac.ebi.part1.dao;

import uk.ac.ebi.part1.models.Taxon;

/**
 * Created by melih on 2/2/2015.
 */
public interface TaxonService {
    Taxon getById(String id);
}
