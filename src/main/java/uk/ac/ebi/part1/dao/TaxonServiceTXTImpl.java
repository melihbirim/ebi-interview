package uk.ac.ebi.part1.dao;

import uk.ac.ebi.part1.models.Taxon;
import uk.ac.ebi.part3.BaseTaxonReader;
import uk.ac.ebi.part3.HashBasedTaxonReader;

/**
 * Created by melih on 2/2/2015.
 */
public class TaxonServiceTXTImpl implements TaxonService {

    private volatile BaseTaxonReader taxonCache;

    private volatile static TaxonService instance;


    private TaxonServiceTXTImpl() {
        //taxonCache = new ArrayBasedSolution();
        taxonCache = new HashBasedTaxonReader();
    }
    public static TaxonService getInstance(){
        if(instance == null){
            instance = new TaxonServiceTXTImpl();
        }
        return instance;
    }

    public Taxon getById(String id) {
        return taxonCache.getById(id);
    }

}
