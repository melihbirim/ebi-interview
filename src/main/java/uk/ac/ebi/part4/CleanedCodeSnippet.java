package uk.ac.ebi.part4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by melih on 2/2/2015.
 */
public class CleanedCodeSnippet {

    static final String SELECT_EMAIL_ADDRESSES = "SELECT EMAIL_ADDRESS FROM SUBMISSION_CONTACT WHERE SUBMISSION_ACCOUNT_ID=?";

    /**
     * Oldschool way to close connections with a util class, provides more readability f
     * @param submissionAccountId id of account at DB
     * @param con DB connection
     * @return email addresses of given account id
     */
    public List<String> fetchContactEmailAddresses(String submissionAccountId, Connection con) {

        List<String> emailAddresses = new ArrayList<>();

        ResultSet res = null;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(SELECT_EMAIL_ADDRESSES);

            stmt.setString(1, submissionAccountId);

            res = stmt.executeQuery();

            while (res.next()) {
                emailAddresses.add(res.getString("EMAIL_ADDRESS"));
            }

        } catch (SQLException e) {
            // No need to roll back because it is just an select statement
            System.err.println(e.getMessage());
        } finally {
            JDBCUtils.closeAll(con, res, stmt);
        }

        return emailAddresses;
    }

    /**
     * Java 7 way of closing resources
     * @param submissionAccountId id of account at DB
     * @param con connection
     * @return email addresses of given account id
     */
    public List<String> fetchContactEmailAddressesJava7CloseStream(String submissionAccountId, Connection con) {

        List<String> emailAddresses = new ArrayList<>();

        try (Connection c = con) {
            try (PreparedStatement stmt = c.prepareStatement(SELECT_EMAIL_ADDRESSES)) {

                stmt.setString(1, submissionAccountId);

                try (ResultSet res = stmt.executeQuery()) {
                    while (res.next()) {
                        emailAddresses.add(res.getString("EMAIL_ADDRESS"));
                    }
                }
            }
        } catch (SQLException e) {
            //No need to roll back because it is just an select statement
            System.err.println(e.getMessage());
        }

        return emailAddresses;
    }
}


class JDBCUtils {
    public static void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public static synchronized void closeAll(Connection connection, ResultSet rs, PreparedStatement st) {
        closeConnection(connection);
        closeResultSetAndPStatement(rs, st);
    }

    public static synchronized void closeConnection(Connection connection) {
        try {
            if ((connection != null) && (!connection.isClosed())) {
                connection.close();
            }
        } catch (Exception exception) {
            System.err.println(exception.getMessage());
        }
    }

    public static void closeResultSetAndPStatement(ResultSet rs, PreparedStatement st) {
        try {
            if (rs != null) rs.close();
            if (st != null) st.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
